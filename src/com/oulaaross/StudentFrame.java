package com.oulaaross;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Formatter;

import static com.oulaaross.ExplorerFrame.defaultListModel;
import static com.oulaaross.ExplorerFrame.fileWriter;
import static com.oulaaross.ExplorerFrame.studentsJList;


/**
 * @author Oulaaross
 */
public class StudentFrame extends JDialog {

    private final GridBagLayout layout;
    private final GridBagConstraints constraints;

    private JLabel image;

    private JLabel heightLabel;
    private JLabel weightLabel;
    private JSlider heightSlider;
    private JSlider weightSlider;

    private JLabel bioFName;
    private JLabel bioCNE;
    private JTextField cneText;
    public JTextField fNameText;
    private JTextField lNameText;
    private JTextField ageText;
    private JLabel bioLName;
    private JLabel bioAge;

    private JButton cancelButton;
    private JButton okButton;
    private JButton editButton;
    private JButton ajoutButton;
    private JButton prevButton;
    private JButton nextButton;
    private Student stu=null;

    public StudentFrame(JFrame parent, String title, boolean bool1, boolean bool2,boolean bool3, int i) {

        super(parent, title, true);

        // on utilise un GridBagLayout
        layout=new GridBagLayout();
        setLayout(layout);
        constraints=new GridBagConstraints();

        // si l'argument i>=0 on recupere l'objet Student associé
        if (i>=0){
            stu=defaultListModel.get(i);
        }

        // integré une photo dans le JDialog
        ImageIcon photo=new ImageIcon(getClass().getResource("student.png"));
        Image imagee = photo.getImage(); // transform it
        Image newimg = imagee.getScaledInstance(90, 90,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
        photo = new ImageIcon(newimg);  // transform it back

        bioFName=new JLabel("Prénom:");
        bioLName=new JLabel("Nom:");
        bioAge=new JLabel("Age:");
        bioCNE=new JLabel("CNE:");

        fNameText=new JTextField(15);
        lNameText=new JTextField(15);
        ageText=new JTextField(15);
        cneText=new JTextField(15);

        image=new JLabel(photo, SwingConstants.LEFT);

        heightSlider=new JSlider(SwingConstants.VERTICAL, 0, 200, 150);
        weightSlider =new JSlider(SwingConstants.HORIZONTAL, 60, 120, 70);
        heightLabel=new JLabel("Taille");
        weightLabel=new JLabel("Poids");
        heightSlider.setMajorTickSpacing(10);
        heightSlider.setPaintTicks(true);
        weightSlider.setMajorTickSpacing(5);
        weightSlider.setPaintTicks(true);


        cancelButton=new JButton("Annuler");
        okButton=new JButton("OK");
        ajoutButton=new JButton("Ajouter");
        editButton=new JButton("Editer");
        prevButton=new JButton("prec");
        nextButton=new JButton("suiv");
        prevButton.setEnabled(bool1);
        nextButton.setEnabled(bool1);

        constraints.weightx=1;
        constraints.weighty=1;
        constraints.fill=GridBagConstraints.BOTH;

        addComponents(image, 0, 0, 2, 4);

        constraints.weightx=0;
        constraints.weighty=0;
        constraints.fill=GridBagConstraints.HORIZONTAL;

        addComponents(fNameText, 0, 3, 1, 1);
        addComponents(lNameText, 1, 3, 1, 1);
        addComponents(ageText, 2, 3, 1, 1);
        addComponents(cneText, 3, 3, 1, 1);
        addComponents(bioFName, 0, 2, 1, 1);
        addComponents(bioLName, 1, 2, 1, 1);
        addComponents(bioAge, 2, 2, 1, 1);
        addComponents(bioCNE, 3, 2, 1, 1);

        constraints.fill=GridBagConstraints.BOTH;

        addComponents(heightLabel, 4, 0, 1, 1);
        addComponents(heightSlider, 4, 1, 1, 1);
        addComponents(weightLabel, 4, 2, 2, 1);
        addComponents(weightSlider, 4, 3, 2, 1);

        constraints.fill=GridBagConstraints.HORIZONTAL;

        if (bool2) {

            addComponents(cancelButton, 5, 1, 1, 1);
        }
        addComponents(prevButton, 5, 0, 1, 1);
        addComponents(nextButton, 5, 3, 1, 1);
        if (bool1){

            addComponents(okButton, 5, 2, 1, 1);
        }
        else {
            addComponents(ajoutButton, 5, 2, 1, 1);
        }
        if (bool3){
            ajoutButton.setVisible(false);
            addComponents(editButton, 5, 2, 1, 1);
        }

        // si l'objet associé a l'index i est !null on integre les valeurs dans les champs
        if (stu!=null){

            cneText.setText(String.valueOf(stu.getCNE()));
            ageText.setText(String.valueOf(stu.getAge()));
            fNameText.setText(stu.getPrenom());
            lNameText.setText(stu.getNom());
            heightSlider.setValue(stu.getTaille());
            weightSlider.setValue(stu.getPoids());
        }

        // dans le cas de Voir la modification n'est pas autorisée
        if (!bool2){
            cneText.setEnabled(false);
            ageText.setEnabled(false);
            fNameText.setEnabled(false);
            lNameText.setEnabled(false);
            heightSlider.setEnabled(false);
            weightSlider.setEnabled(false);
        }

        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                System.out.println("ok");

                if (i>=0 && i<defaultListModel.size()-1){

                    Student nextStudent=defaultListModel.get(i+1);
                    cneText.setText(String.valueOf(nextStudent.getCNE()));
                    ageText.setText(String.valueOf(nextStudent.getAge()));
                    fNameText.setText(nextStudent.getPrenom());
                    lNameText.setText(nextStudent.getNom());
                    heightSlider.setValue(nextStudent.getTaille());
                    weightSlider.setValue(nextStudent.getPoids());

                }


            }
        });

        prevButton.setEnabled(false);

        // un Listener pour ajouter un etudiant
        ajoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int cne=Integer.parseInt(cneText.getText());
                String nom=lNameText.getText();
                String prenom=fNameText.getText();
                int age=Integer.parseInt(ageText.getText());
                int taille=heightSlider.getValue();
                int poids=weightSlider.getValue();

                try {
                    fileWriter = new FileWriter("students.txt", true);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                ExplorerFrame.output=new Formatter(fileWriter);

                // enregistrer les valeurs des champs dans le fichier students.txt
                ExplorerFrame.output.format("%d %s %s %d %d %d%n", cne, nom, prenom, age, taille, poids);

                // fermer le formatter
                if (ExplorerFrame.output!=null)
                    ExplorerFrame.output.close();

                // ajouter l'etudiant a la defaultListModel, JList va changer automatiquement
                defaultListModel.addElement(new Student(cne, nom, prenom, age, taille, poids));

                setVisible(false);
            }
        });

        // changer les infos d'un etudiant deja present dans la liste
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // on va d'abord supprimer l'etudiant de la defaultListModel
                // et supprimer la ligne dans le fichier students.txt contenant les infos de l'etudiant
                // avant d'ajouter les champs contenants les modifications

                Student stt= studentsJList.getSelectedValue();

                // supprimer l'etudiant
                defaultListModel.removeElement(stt);
                int cne=Integer.parseInt(cneText.getText());
                String nom=lNameText.getText();
                String prenom=fNameText.getText();
                int age=Integer.parseInt(ageText.getText());
                int taille=heightSlider.getValue();
                int poids=weightSlider.getValue();

                // ajouter l'element Student avec les nouvelles valeurs
                defaultListModel.addElement(new Student(cne, nom, prenom, age, taille, poids));

                try{

                    // fermer le fileWriter et input pour eviter les problemes lie a la suppression et la
                    // modification du nom de fichier
                    ExplorerFrame.fileWriter.close();
                    ExplorerFrame.input.close();

                    File inFile = new File("students.txt");

                    // le mécanisme pour supprimer la ligne est de creer un fichier temporaire
                    // students2.txt et de copier les lignes sauf la ligne qu'on veut supprimer
                    // et enfin supprimer le fichier originale et modifier le nom du fichier students2.txt à student.txt
                    File tempFile = new File("students2.txt");

                    // la ligne à supprimer
                    String lineToRemove=String.format("%d %s %s %d %d %d", stt.getCNE(), stt.getNom(), stt.getPrenom(),
                            stt.getAge(), stt.getTaille(), stt.getPoids());

                    BufferedReader br = new BufferedReader(new FileReader(inFile));
                    PrintWriter pw = new PrintWriter(new FileWriter(tempFile));

                    String line = null;

                    while ((line = br.readLine()) != null) {

                        if (!line.trim().equals(lineToRemove)) {

                            pw.println(line);
                            pw.flush();
                        }
                    }
                    pw.close();
                    br.close();

                    // supprimer le fichier originale
                    if (!inFile.delete()) {
                        System.out.println("Erreur. Suppression impossible");
                        return;
                    }

                    //changer le nom de students2 à students
                    if (!tempFile.renameTo(inFile))
                        System.out.println("Erreur.");

                }
                catch (Exception e1){
                    e1.printStackTrace();
                }
                try {
                    fileWriter = new FileWriter("students.txt", true);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                ExplorerFrame.output=new Formatter(fileWriter);

                // enfin ajouter le nouveau Student au fichier

                ExplorerFrame.output.format("%d %s %s %d %d %d%n", cne, nom, prenom, age, taille, poids);
                if (ExplorerFrame.output!=null)
                    ExplorerFrame.output.close();

                setVisible(false);
            }
        });

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(parent);
        setVisible(true);

    }

    /**
     * @param component
     * l'element à ajouter
     * @param row
     * num de la ligne
     * @param column
     * num de la colonne
     * @param width
     * nombre de cases horizontales
     * @param height
     * nombre de cases verticales
     */
    private void addComponents(Component component, int row, int column, int width, int height){

        constraints.gridx=column;
        constraints.gridy=row;
        constraints.gridwidth=width;
        constraints.gridheight=height;
        layout.setConstraints(component, constraints);
        add(component);
    }

}










