package com.oulaaross;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * @author Oulaaross
 */
public class RemoveListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {

        try {

            // le mécanisme pour supprimer la ligne est de creer un fichier temporaire
            // students2.txt et de copier les lignes sauf la ligne qu'on veut supprimer
            // et enfin supprimer le fichier originale et modifier le nom du fichier students2.txt à student.txt

            Student sttd= ExplorerFrame.studentsJList.getSelectedValue();

            // on va d'abord supprimer l'etudiant de la defaultListModel
            ExplorerFrame.defaultListModel.remove(ExplorerFrame.studentsJList.getSelectedIndex());

            // fermer le fileWriter, output et input pour eviter les problemes lié a la suppression et la
            // modification du nom de fichier
            ExplorerFrame.fileWriter.close();
            ExplorerFrame.input.close();
            ExplorerFrame.output.close();

            File inFile = new File("students.txt");
            File tempFile = new File("students2.txt");

            String lineToRemove=String.format("%d %s %s %d %d %d", sttd.getCNE(), sttd.getNom(), sttd.getPrenom(),
                    sttd.getAge(), sttd.getTaille(), sttd.getPoids());

            BufferedReader br = new BufferedReader(new FileReader(inFile));
            PrintWriter pw = new PrintWriter(new FileWriter(tempFile));

            String line = null;

            while ((line = br.readLine()) != null) {

                if (!line.trim().equals(lineToRemove)) {

                    pw.println(line);
                    pw.flush();
                }
            }
            pw.close();
            br.close();

            // supprimer le fichier originale
            if (!inFile.delete()) {
                System.out.println("Erreur. Suppression impossible");
                return;
            }

            //changer le nom de students2 à students
            if (!tempFile.renameTo(inFile))
                System.out.println("Erreur.");




        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
