package com.oulaaross;


/**
 * @author Oulaaross
 */
public class Student {

    private int CNE=0;
    private String Nom="";
    private String Prenom="";
    private int Taille=0;
    private int Poids=0;
    private int Age=0;

    public Student(){
        this(0, "", "", 0, 0, 0);
    }

    public Student(int CNE, String nom, String prenom, int age, int taille, int poids) {
        this.CNE = CNE;
        Nom = nom;
        Prenom = prenom;
        Taille = taille;
        Age=age;
        Poids=poids;
    }

    // on vas afficher cette methode toString dans JList
    @Override
    public String toString() {
        return CNE+" "+Prenom+" "+Nom;
    }

    // les Getters

    public int getCNE() {
        return CNE;
    }

    public void setCNE(int CNE) {
        this.CNE = CNE;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    public int getTaille() {
        return Taille;
    }

    public void setTaille(int taille) {
        Taille = taille;
    }

    public int getPoids() {
        return Poids;
    }

    public void setPoids(int poids) {
        Poids = poids;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }
}
