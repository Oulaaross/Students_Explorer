package com.oulaaross;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.Scanner;

/**
 * @author Oulaaross
 */
public class ExplorerFrame extends JFrame {

    public static Formatter output;
    public static Scanner input;
    public static FileWriter fileWriter;

    public static JList<Student> studentsJList;
    public static DefaultListModel<Student> defaultListModel;

    private JButton addStudent;
    private JButton removeStudent;
    private JButton editStudent;
    private JButton viewStudent;
    private JButton exitButton;

    private JPanel buttonsPanel;

    public ExplorerFrame() throws IOException{

        super("Guide des étudiants");
        defaultListModel=new DefaultListModel<>();

        try {

            // un new FileWriter avec 2 arguments: 1er arg est le fichier, s'elle n'existe pas elle va etre creé
            // le 2em arg pour indiquer qu'on veut ajouter au contenu du fichier et pas écraser ce dernier
            fileWriter = new FileWriter("students.txt", true);
            output=new Formatter(fileWriter);
            input=new Scanner(Paths.get("students.txt"));

            // un boucle while pour ajouter des Objects Students à DefaultListModel a partir des donnees du fichier
            // students.txt
            while (input.hasNext()){
                defaultListModel.addElement(new Student(input.nextInt(), input.next(), input.next(), input.nextInt(),
                        input.nextInt(), input.nextInt()));
            }


        }
        catch (Exception e){
            e.printStackTrace();
        }

        studentsJList=new JList<>(defaultListModel);

        // indiquer le mode de selection des elements de la JList
        studentsJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        addStudent=new JButton("Ajouter");
        editStudent=new JButton("Editer");
        removeStudent=new JButton("Supprimer");
        viewStudent=new JButton("Voir");
        exitButton=new JButton("Fermer");

        buttonsPanel=new JPanel();
        buttonsPanel.add(addStudent);
        buttonsPanel.add(removeStudent);
        buttonsPanel.add(editStudent);
        buttonsPanel.add(viewStudent);
        buttonsPanel.add(exitButton);

        add(new JScrollPane(studentsJList), BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.SOUTH);



        // un Listener associé au button Ajouter
        addStudent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // instancier un JDialog pour remplir les infos
                new StudentFrame(ExplorerFrame.this, "Ajouter un étudiant", false, true, false, -1);
            }
        });

        // un Listener associé au button Voir
        viewStudent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // instancier un JDialog en passant l'index selectionné ds les args pour
                // afficher les valeurs dans les textfields
                int i=studentsJList.getSelectedIndex();
                StudentFrame std=new StudentFrame(ExplorerFrame.this, "Profile", true, false, false, i);
            }
        });

        // un Listener au btn Supprimer
        removeStudent.addActionListener(new RemoveListener());

        // un Listener associé au button Editer
        editStudent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // instancier un JDialog en passant l'index selectionné ds les args pour
                // afficher les valeurs dans les textfields
                int j=studentsJList.getSelectedIndex();
                StudentFrame std=new StudentFrame(ExplorerFrame.this, "Profile", false, true, true, j);

            }
        });

        // fermer l'application
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });


    }

    public static void main(String[] args) throws IOException {

        ExplorerFrame explorerFrame=new ExplorerFrame();
        explorerFrame.setSize(600, 400);
        explorerFrame.setVisible(true);
        explorerFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

}























